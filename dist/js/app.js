$(document).ready(function () {

  $('.left-nav .sub-list span').click(function () {
    $(this).next('ul').show('fast')
    if ($(this).hasClass('active')) {
      $(this).next('ul').hide('fast')
    }
    $(this).toggleClass('active');
  })

  $('.m-header-bottom_open').on('click', function () {
    $('.m-header-nav').slideToggle('fast');
  })

  $('.filter-btn').on('click', function () {
    $(this).toggleClass('active');
    $('.filter-block').slideToggle('fast');
  })

  $('.owl-news').owlCarousel({
    items: 1,
    dots: false,
    nav:true,
    margin: 25,
    padding: 10,
    loop: true,
    navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #fff;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>','<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #fff;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
    autoHeight: true,
    // responsive: {
    //   0: {
    //     autoHeight: true,
    //   },
    //   768: {
    //     autoHeight: false,
    //   }
    // }
  });
});
